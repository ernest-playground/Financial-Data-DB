from .base import Base

from sqlalchemy import Table, Column, Integer, String, MetaData, ForeignKey, BINARY, FLOAT, BOOLEAN, DECIMAL


# Time Series data
class PriceData(Base):
    __tablename__ = 'price_data'
