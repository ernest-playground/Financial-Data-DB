import os

import yaml
from sql_database import Base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session


with open('./config.yml') as file:
    content = yaml.load(file, Loader=yaml.FullLoader)

db_dir = content['database_directory']


# def create_db(name='sqlite:///./history.db'):
#     database_engine = create_engine(name)
#     Session = sessionmaker(bind=database_engine)
#     database_session = Session()
#
#     Base.metadata.drop_all(bind=database_engine)
#     Base.metadata.create_all(bind=database_engine)
#
#     database_session.commit()
#
#
# if not os.path.exists(db_dir):
#     create_db(f'sqlite:///{db_dir}')


database_engine = create_engine(db_dir, echo=True)
database_session = scoped_session(sessionmaker(autocommit=False, autoflush=False, bind=database_engine))
Base.query = database_session.query_property()
